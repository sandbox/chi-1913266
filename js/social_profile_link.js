(function($) {

/**
 * Social profile link select widget.
 *
 * @see http://tutorialzine.com/2010/11/better-select-jquery-css3/
 */
Drupal.behaviors.social_profile_link = {
  attach: function (context, settings) {
  
  var icons = Drupal.settings.social_profile_link.icons
  var iconsDir = settings.social_profile_link.icons_dir

  $('select.spl-services:not(.processed)').each(function() {
    var select = $(this);
    select.addClass('processed');

    var selectBoxContainer = $('<div>',{
      // width: select.outerWidth(),
      className: 'spl-select',
      html: '<div class="spl-box"></div>'
    });

    var dropDown = $('<ul>', {className: 'spl-dropdown', id: 'spl-' + select.attr('id')});
    var selectBox = selectBoxContainer.find('.spl-box');
    var urlField = select.parents('.social-profile-link-elements').find('input')
    urlField.hide()
    
    selectBox.update = function(option) {
      selectBox.html('<div>' + option.text() + '</div>')
      if (option.val() == '_none') {
        selectBox.css('background-image', 'none');
        urlField.removeAttr('placeholder').hide().val('')
        selectBox.addClass('spl-empty');
      }
      else {
        selectBox.css('background-image',  'url(' + iconsDir + '/' + icons[option.val()] + ')');
        urlField.attr('placeholder', 'Link to your ' + option.text() + ' profile').show()
        selectBox.removeClass('spl-empty');
      }
    }
    
    // Looping though the options of the original select element
    select.find('option').each(function(i) {
      var option = $(this);

      if (i == select.attr('selectedIndex')){
        selectBox.update(option)
      }

      var li = $('<li>', {html:  '<div>' + option.text() + '</div>'});
      if (option.val() == '_none') {
        li.addClass('spl-empty');
      }
      else {
        li.css('background-image', 'url(' + iconsDir + '/' + icons[option.val()] + ')')
      }
      li.click(function() {

        selectBox.update(option)
        dropDown.trigger('hide');

        // When a click occurs, we are also reflecting
        // the change on the original select element:
        select.val(option.val());
        return false;
      });

      dropDown.append(li);
    });

    selectBoxContainer.append(dropDown.hide());
    select.hide().after(selectBoxContainer);

    // Binding custom show and hide events on the dropDown:
    dropDown.bind('show', function(){

      if(dropDown.is(':animated')){
        return false;
      }

      selectBox.addClass('expanded');
      dropDown.slideDown(150);

    }).bind('hide',function(){

      if(dropDown.is(':animated')){
        return false;
      }

      selectBox.removeClass('expanded');
      dropDown.slideUp(150);

    }).bind('toggle',function(){
      if(selectBox.hasClass('expanded')){
        dropDown.trigger('hide');
      }
      else dropDown.trigger('show');
    });

    selectBox.click(function() {
      $('ul.spl-dropdown:not(#' + dropDown.attr('id') + ')')
        .hide()
        .prev()
        .removeClass('expanded')
      dropDown.trigger('toggle');
      return false;
    });

    // If we click anywhere on the page, while the
    // dropdown is shown, it is going to be hidden:
    $(document).click(function(){
      dropDown.trigger('hide');
    });
  });

  }
};

})(jQuery);
